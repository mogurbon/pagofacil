<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;



class PagoController extends Controller
{
    public function register(Request $request) {

            $fecha_registro= date("Y-m-d");
            $calificacion= $request->input('calificacion');
            $materia= $request->input('materia');

        DB::beginTransaction();
        try {
            if(is_numeric($calificacion) and is_numeric($materia)){
                if (DB::table('t_calificaciones')->insert([
                    'id_t_materias' => $materia,
                    'id_t_usuarios' => 1,
                    'calificacion' => $calificacion,
                    'fecha_registro' => $fecha_registro
                ])) {
                    DB::commit();
                    echo '{"success":"ok", "msg":"calificacion registrada"}';
                }
            }
            else{
                return "Wrong data";
            }
        }catch (\Exception $e){
            DB::rollback();
            return  $e->getMessage();
        }
    }
    public function listAverage($id) {


        try{
            if ($id) {
                $datos = DB::table('t_calificaciones')
                    ->join('t_alumnos', 't_calificaciones.id_t_usuarios', '=', 't_alumnos.id')
                    ->join('t_materias', 't_calificaciones.id_t_materias', '=', 't_materias.id_t_materias')
                    ->select('t_alumnos.id', 't_alumnos.nombre', 't_alumnos.ap_paterno', 't_materias.nombre as materia',
                        't_calificaciones.calificacion', 't_calificaciones.fecha_registro')
                    ->where('t_alumnos.id', $id)
                    ->get();
                $num = 0;
                $suma = 0;
                if (count($datos) > 0) {

                    foreach ($datos as $dato) {
                        $array[] = [
                            'id_t_usuario' => $dato->id,
                            'nombre' => $dato->nombre,
                            'apellido' => $dato->ap_paterno,
                            'materia' => $dato->materia,
                            'calificacion' => $dato->calificacion
                        ];
                        $suma += $dato->calificacion;
                        $num++;
                    }


                    array_push($array,['promedio'=>$suma/$num]);
                    return json_encode($array);
                }
                else{
                    return "Id does not Exist";
                }
            }

        } catch (\Exception $e) {
            return  $e->getMessage();
        }
    }
    public function update($calificacion,$id_t_calificaciones) {
        DB::beginTransaction();
        try{
            if (is_numeric($calificacion) and is_numeric($id_t_calificaciones)){
                if (DB::table('t_calificaciones')
                    ->where('id_t_calificaciones', $id_t_calificaciones)
                    ->update(['calificacion' => $calificacion])){
                    DB::commit();
                        echo '{"success":"ok", "msg":"calificacion actualizada”}';
                }
            }
            else{
                return "Wrong data";
            }
        }catch (\Exception $e){
            DB::rollback();
            $e->getMessage();
        }
    }
    public function delete($id_t_calificaciones) {
        DB::beginTransaction();
        try{
            DB::table('t_calificaciones')->where('id_t_calificaciones', $id_t_calificaciones)->delete();
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            $e->getMessage();
        }
    }
}