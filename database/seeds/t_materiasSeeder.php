<?php

use Illuminate\Database\Seeder;

class t_materiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materias=[['nombre'=>'matematicas','activo'=>1],['nombre'=>'programacion I','activo'=>1],['nombre'=>'ingenieria de sofware','activo'=>1]];
        foreach ($materias as $materia){
            DB::table('t_materias')->insert($materia);
        }
    }
}
