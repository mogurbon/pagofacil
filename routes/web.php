<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/list/{id}', 'PagoController@listAverage');
Route::put('/update/{calificacion}/{id_t_calificaciones}', 'PagoController@update');
Route::delete('/delete/{id_t_calificaciones}', 'PagoController@delete');
Route::post('/register', [
     'uses' => 'PagoController@register',
         'as' => 'register'
    ]);




    


